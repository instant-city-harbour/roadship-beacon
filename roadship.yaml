### This section is for both the server and the client
- hosts: all
  become: yes
  tasks:
  - name: "Set hostnames"
    hostname: name={{ inventory_hostname }}

  - name: "Installing packages"
    package:
     name: "{{ item }}"
     state: present
     update_cache: true
    loop:
    - quagga
#    - strongswan
    - bind9
#    - bridge-utils
    - less
    - git
    - rsync
    - mc
    - nmap
    - dnsutils
    - tcpdump
    - bzip2
    - locales
    - apache2
    - tinc
    - sudo
    - psmisc
    - ntp
    - freeradius
    - freeradius-utils

  - name: "Create empty quagga config files (if needed)"
    copy:
     content: ""
     dest: "/etc/quagga/{{ item }}.conf"
     force: no
     group: quagga
     owner: quagga
     mode: 0640
    with_items: "{{ QUAGGADAEMONS }}"

  - name: "Enable Quagga daemons"
    service:
     name: "{{ item }}"
     enabled: yes
    with_items: "{{ QUAGGADAEMONS }}"

  - name: "Start Quagga daemons"
    service:
     name: "{{ item }}"
     state: started
    with_items: "{{ QUAGGADAEMONS }}"

  - name: Generating locales
    locale_gen:
     name: "{{ item }}"
     state: present
    loop:
    - fr_BE.UTF-8
    - nl_BE.UTF-8
    - de_BE.UTF-8
    - fi_FI.UTF-8
    - en_GB.UTF-8

  - name: "Create tinc directories"
    file:
     path: "/etc/tinc/{{ item }}/hosts"
     state: directory
     recurse: yes
    with_items: "{{ networktechnologies }}"

# Generate tinc keys. Skip this step if keys were previously generated.
  - name: "Generate tinc keys"
    command: tincd -n {{ item }} -K 4096
    args:
     creates: "/etc/tinc/{{ item }}/rsa_key.priv"
    with_items: "{{ networktechnologies }}"

  - name: "Create UP script for tinc interfaces"
    copy:
     dest: "/etc/tinc/{{ item }}/tinc-up"
     content: "#!/bin/bash\n# Created by Ansible\nip link set dev $NETNAME up\n"
     mode: 0755
    with_items: "{{ networktechnologies }}"

  - name: "Copy local public key to definitive local destination"
    copy:
     src: "/etc/tinc/{{ item.1 }}/rsa_key.pub"
     dest: "/etc/tinc/{{ item.1 }}/hosts/{{ (inventory_hostname) | replace ('.','_') }}"
     backup: yes
     remote_src: yes
    with_indexed_items: "{{ networktechnologies }}"

  - name: "Fetching all public keys"
    fetch:
     dest: "publickeys/"
     src: "/etc/tinc/{{ item.0 }}/rsa_key.pub"
    with_nested:
     - "{{ networktechnologies }}"
     - "{{ query('inventory_hostnames', 'all') }}"

  - name: "Preparing for Wireguard installation: adding unstable source (Debian)"
    copy:
     dest: "/etc/apt/sources.list.d/unstable-wireguard.list"
     backup: yes
     content: "# Created by Ansible playbook\ndeb http://deb.debian.org/debian/ unstable main"
    when: ansible_distribution == 'Debian'

  - name: "Preparing for Wireguard installation: Pinning unstable (Debian)"
    copy:
     dest: "/etc/apt/preferences.d/limit-unstable"
     backup: yes
     content: "# Created by Ansible playbook\nPackage: *\nPin: release a=unstable\nPin-Priority: 90\n"
    when: ansible_distribution == 'Debian'

  - name: "Preparing for Wireguard installation: adding repository (Ubuntu)"
    command: echo | add-apt-repository ppa:wireguard/wireguard
    when: ansible_distribution == 'Ubuntu'

  - name: "Preparing for Wireguard installation: updating package cache (Debian/Ubuntu)"
    command: apt-get update
    when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

  - name: "Install Wireguard"
    package:
     name: wireguard
     state: present
     update_cache: true

#  - name: "Generate Wireguard keys"
#    command: tincd -n {{ item }} -K 4096
#    args:
#     creates: "/etc/tinc/{{ item }}/rsa_key.priv"
#    with_items: "{{ networktechnologies }}"

  - name: "Activating IP forwarding with Quagga"
    shell: |
     configure terminal
     ip forwarding
     ipv6 forwarding
     end
     write
    args:
     executable: /usr/bin/vtysh

###########################################################################################################################

- hosts: roadship-client
  become: yes
  vars:
   networktechnologies_indexes:
  tasks:
  - name: "Add interfaces to /etc/network/interfaces file"
    lineinfile:
     path: "/etc/network/interfaces"
     line: "iface {{ item.1 }}_in inet manual"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: Generate namespaces entries"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: up
     value: "ip netns add {{ item.1 }}"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: Generate veth pairs"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: up
     value: "ip link add {{ item.1 }}_in type veth peer name {{ item.1 }}_out"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: put 'out' interface into namespace"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: up
     value: "ip link set dev {{ item.1 }}_out netns {{ item.1 }}"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: Bring 'out' interfaces up"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: up
     value: "ip netns exec {{ item.1 }} ip link set dev {{ item.1 }}_out up"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: Namespace teardown"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: down
     value: "ip netns del {{ item.1 }}"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: veth teardown"
    interfaces_file:
     dest: "/etc/network/interfaces"
     iface: "{{ item.1 }}_in"
     option: down
     value: "ip link del {{ item.1 }}_in"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

  - name: "Restarting interfaces"
    command: "bash -c 'ifdown {{ item.1 }}_in ; sleep 4 ; ifup {{ item.1 }}_in'"
    with_indexed_items: "{{ networktechnologies }}"

  - name: "/etc/network/interfaces: add automatic start for new interfaces"
    lineinfile:
     path: "/etc/network/interfaces"
     line: "auto {{ item.1 }}"
     insertbefore: "iface {{ item.1 }}_in inet static"
     state: present
    with_indexed_items: "{{ networktechnologies }}"

# The namespaces should be UP when this section is being run. If they are not, the statements below will be IGNORED
  - name: "Quagga: associating network namespace with VLAN number"
    shell: |
     configure terminal
     vrf {{ wan_vlan_base + item.0 }} netns {{ item.1 }}
     end
     write
    args:
     executable: /usr/bin/vtysh
    with_indexed_items: "{{ networktechnologies }}"

  - name: "Generate tinc config files (client)"
    copy:
     dest: "/etc/tinc/{{ item.1 }}/tinc.conf"
     backup: yes
     content: "# Created by Ansible playbook (client)\nName = {{ (inventory_hostname) | replace ('.','_') }}\nMode = Switch\nPort = {{ item.0 + base_port }}"
    with_indexed_items: "{{ networktechnologies }}"

  - name: "Patch client configs: add server(s) to connect to"
    lineinfile: dest="/etc/tinc/{{ item[0] }}/tinc.conf" line="ConnectTo={{ (item[1]) | replace ('.','_') }}"
    with_nested:
     - "{{ networktechnologies }}"
     - "{{ query('inventory_hostnames', 'roadship-server') }}"

  - name: "Pushing server(s) public keys to client(s)"
    copy:
     src: "publickeys/{{ item.1 }}/etc/tinc/{{ item.0 }}/rsa_key.pub"
     dest: "/etc/tinc/{{ item.0 }}/hosts/{{ (item.1) | replace ('.','_') }}"
     backup: yes
    with_nested:
     - "{{ networktechnologies }}"
     - "{{ query('inventory_hostnames', 'roadship-server') }}"

  - name: "Activating connections to start automatically (client)"
    command: systemctl enable tinc@{{ item }}
    with_items: "{{ networktechnologies }}"

  - name: "Starting tinc instances (client)"
    command: systemctl start tinc@{{ item }}
    with_items: "{{ networktechnologies }}"

  - name: "Force re-start tinc instances (client)"
    command: systemctl restart tinc@{{ item }}
    with_items: "{{ networktechnologies }}"

  - name: "Configuring tunnel interfaces IPs with Quagga"
    shell: |
     configure terminal
     interface {{ item.1 }}
     ip address {{ TUNNELBLOCK | ipaddr('-1') | ipsubnet (TUNNELBLOCKSIZE, -(item.0 + 1) ) | ipaddr( 1 + groups['roadship-client'].index(inventory_hostname) )  }}
     ip ospf message-digest-key 1 md5 {{ OSPFKEY }}
     end
     write
    args:
     executable: /usr/bin/vtysh
    with_indexed_items: "{{ networktechnologies }}"

#  - name: "Ansible limitation workaround - Convert networktechnologies array into a list of indexes"
#    set_fact: 
#      networktechnologies_indexes: "{{ networktechnologies_indexes + [ item.0 ] }}"
#    with_indexed_items: "{{ networktechnologies }}"
##    register: networktechnologies_indexes

### BUG: this shit is needed but DOES NOT WORK
#  - name: "Patching public key with destination port"
##    lineinfile: dest="/etc/tinc/{{ item[0] }}/hosts/{{ (item.1) | replace ('.','_') }}" line="Port={{ item.0.index + base_port }}"
#    lineinfile: 
#     path: "/etc/tinc/{{ item.0 }}/hosts/{{ (item.1) | replace ('.','_') }}" 
#     line: "Port={{ item.0.index }}"
#     insertbefore: BOF
##    with_indexed_items: 
##     - "{{ networktechnologies }}"
##     - "{{ query('inventory_hostnames', 'roadship-server') }}"
#    with_nested:
#     - "{{ networktechnologies }}"
#     - "{{ query('inventory_hostnames', 'roadship-server') }}"

  - name: "Installing packages"
    package:
     name: "{{ item }}"
     state: present
     update_cache: true
    loop:
    - ifenslave
    - isc-dhcp-server
    - snmpd
    - bridge-utils

###########################################################################################################################

- hosts: roadship-server
  become: yes
  tasks:
  - name: Generate tinc config files (server)
    copy:
     dest: "/etc/tinc/{{ item.1 }}/tinc.conf"
     backup: yes
     content: "# Created by Ansible playbook (server)\nName = {{ (inventory_hostname) | replace ('.','_') }}\nMode = Switch\nPort = {{ item.0 + base_port }}"
    with_indexed_items: "{{ networktechnologies }}"

  - name: "Pushing client(s) public keys to server(s)"
    copy:
     src: "publickeys/{{ item.1 }}/etc/tinc/{{ item.0 }}/rsa_key.pub"
     dest: "/etc/tinc/{{ item.0 }}/hosts/{{ (item.1) | replace ('.','_') }}"
    with_nested:
     - "{{ networktechnologies }}"
     - "{{ query('inventory_hostnames', 'roadship-client') }}"

  - name: "Activating connections to start automatically (server)"
    command: systemctl enable tinc@{{ item }}
    with_items: "{{ networktechnologies }}"

  - name: "Starting tinc instances (server)"
    command: systemctl start tinc@{{ item }}
    with_items: "{{ networktechnologies }}"

#  - name: "Force re-start tinc instances (server)"
#    command: systemctl restart tinc@{{ item }}
#    with_items: "{{ networktechnologies }}"

  - name: "Configuring tunnel interfaces IPs with Quagga"
    shell: |
     configure terminal
     interface {{ item.1 }}
     ip address {{ TUNNELBLOCK | ipaddr('-1') | ipsubnet (TUNNELBLOCKSIZE, -(item.0 + 1) ) | ipaddr( -2 - groups['roadship-server'].index(inventory_hostname) ) }}
     ip ospf message-digest-key 1 md5 {{ OSPFKEY }}
     end
     write
    args:
     executable: /usr/bin/vtysh
    with_indexed_items: "{{ networktechnologies }}"


#  - name: "Assigning IP to VPN interface"

  - name: "Installing packages"
    package:
     name: "{{ item }}"
     state: present
     update_cache: true
    loop:
    - postfix
    - apache2
    - certbot
    - squid
