This is a playbook to deploy a persistent network setup for a mobile station.  
It will handle gracefully transition from wired ethernet to wifi and/or to 4G.

It requires an anchor point (a server) on the internet.

The idea:
```
             +-------+                                   +--------+
             |server |                                   |Mobile  |-- Internal WiFi
             |       |--------- 4G connection -----------|endpoint|-- Guest WiFi
  Internet --|       |--------- WiFi 2,4 GHz ------------|        |-- Wired ethernet
             |       |--------- WiFi 5 GHz --------------|        |-- Embedded devices
             |       |----- Ethernet (stationary use) ---|        |
             |       |                                   |        |
             +-------+                                   +--------+
```
The switchover between the connections should be as smooth as possible, eventually without dropping an established connection.
We will establish several VPNs between the mobile endpoint and the home server. Each VPN will use one of the available uplink
technologies. OSPF will be used to monitor each link, define path preference (you don't want to stay on expensive 3G/4G when 
WiFi is available), and routing between the vehicle network and the outside world. Every connection from the mobile endpoint
will be channeled through the home server.